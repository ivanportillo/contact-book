import next from "next";
import prepareServer from "./server";
import { Application } from "express";
import prepareDatabase from "./database";

const port = 3000;
const isDev = process.env.NODE_ENV !== "production";

const app = next({
  dev: isDev
});

(async () => {
  await app.prepare();

  app.setAssetPrefix(process.env.STATIC_PATH);

  const server: Application = prepareServer();

  const handler = app.getRequestHandler();
  server.get("*", (req, res) => handler(req, res));

  await prepareDatabase();
  server.listen(port);

  console.log(`[SERVER] listening at localhost:${port}`);
})();

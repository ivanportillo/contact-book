import mongoose from "mongoose";

const { MONGO_HOST, MONGO_DATABASE } = process.env;

export default async () => {
  const connection = await mongoose.connect(
    `mongodb://${MONGO_HOST}/${MONGO_DATABASE}`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  );
  console.log("[DATABASE] Successfully connected to the database");

  return connection;
};

import { ContactRepository } from "../domain/ContactRepository";
import ContactId from "../domain/ContactId";
import { IContact } from "./Mongo/ContactModelDocument";
import { Model } from "mongoose";
import ContactMap from "./Mongo/ContactMap";
import Contact from "../domain/Contact";
import { ContactNotFound } from "../domain/ContactNotFound";

class ContactMongoRepository implements ContactRepository {
  private _contactModel: Model<IContact>;

  constructor(contactModel: Model<IContact>) {
    this._contactModel = contactModel;
  }

  async byId(id: ContactId) {
    const query = { _id: id.id };
    const result = await this._contactModel.findOne(query);

    return result ? ContactMap.toDomain(result) : null;
  }

  async byEmail(email: string) {
    const query = { email };
    const result = await this._contactModel.findOne(query);

    return result ? ContactMap.toDomain(result) : null;
  }

  async getAll() {
    const contacts = await this._contactModel.find();

    return contacts.map(ContactMap.toDomain);
  }

  async store(contact: Contact) {
    const contactToPersist = ContactMap.toPersistence(contact);
    await contactToPersist.save();

    return ContactMap.toDomain(contactToPersist);
  }

  async update(id: ContactId, updatedContact: Contact) {
    const query = { _id: id.id };
    const documentToUpdate = await this._contactModel.findOne(query);

    if (!documentToUpdate) {
      throw new ContactNotFound();
    }

    documentToUpdate.name = updatedContact.name;
    documentToUpdate.lastName = updatedContact.lastName;
    documentToUpdate.email = updatedContact.email.email;
    documentToUpdate.phone.number = updatedContact.phone.number;
    documentToUpdate.phone.prefix = updatedContact.phone.prefix;

    await documentToUpdate.save();

    return ContactMap.toDomain(documentToUpdate);
  }

  async remove(id: ContactId) {
    const query = { _id: id.id };

    const removedContact = await this._contactModel.findOneAndDelete(query);

    if (!removedContact) {
      throw new ContactNotFound();
    }

    return;
  }
}

export default ContactMongoRepository;

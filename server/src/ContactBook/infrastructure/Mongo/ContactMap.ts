import Contact from "../../domain/Contact";
import ContactId from "../../domain/ContactId";
import Phone from "../../domain/Phone";
import ContactModelDocument, { IContact } from "./ContactModelDocument";
import Email from "../../domain/Email";

export interface ContactDTO {
  id: string;
  email: string;
  name: string;
  lastName: string;
  phone: {
    prefix: string;
    number: string;
  };
}

class ContactMap {
  public static toDomain(rawContact: IContact): Contact {
    return new Contact(
      new ContactId(rawContact._id),
      rawContact.name,
      rawContact.lastName,
      new Email(rawContact.email),
      new Phone(rawContact.phone.prefix, rawContact.phone.number)
    );
  }

  public static toPersistence(contact: Contact): IContact {
    return new ContactModelDocument({
      _id: contact.id.id,
      email: contact.email.email,
      name: contact.name,
      lastName: contact.lastName,
      phone: {
        prefix: contact.phone.prefix,
        number: contact.phone.number
      }
    });
  }

  public static toDTO(contact: Contact): ContactDTO {
    return {
      id: contact.id.id,
      email: contact.email.email,
      name: contact.name,
      lastName: contact.lastName,
      phone: {
        prefix: contact.phone.prefix,
        number: contact.phone.number
      }
    };
  }

  public static toDomainFromDTO(contact: ContactDTO): Contact {
    return new Contact(
      new ContactId(contact.id),
      contact.name,
      contact.lastName,
      new Email(contact.email),
      new Phone(contact.phone.prefix, contact.phone.number)
    );
  }
}

export default ContactMap;

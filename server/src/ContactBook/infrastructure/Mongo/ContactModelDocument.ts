import { model, Document } from "mongoose";
import ContactSchema from "./ContactSchema";

export interface IContact extends Document {
  _id: string;
  name: string;
  lastName: string;
  email: string;
  phone: {
    prefix: string;
    number: string;
  };
}

export default model<IContact>("Contact", ContactSchema);

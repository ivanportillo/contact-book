import { Schema } from "mongoose";

export default new Schema({
  _id: String,
  name: String,
  lastName: String,
  email: String,
  phone: {
    prefix: String,
    number: String
  }
});

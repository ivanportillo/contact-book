import { ContactRepository } from "../domain/ContactRepository";
import Contact from "../domain/Contact";
import ContactId from "../domain/ContactId";
import ContactMap, { ContactDTO } from "./Mongo/ContactMap";
import { ContactNotFound } from "../domain/ContactNotFound";

export class ContactMemoryRepository implements ContactRepository {
  private _hashData: Map<string, ContactDTO>;

  constructor() {
    this._hashData = new Map();
  }

  async byId(id: ContactId) {
    const contact = this._hashData.get(id.id);

    return contact ? ContactMap.toDomainFromDTO(contact) : null;
  }

  async byEmail(email: string) {
    const contacts = Array.from(this._hashData.values());

    const contact = contacts.find(contact => contact.email === email);

    return contact ? ContactMap.toDomainFromDTO(contact) : null;
  }

  async getAll() {
    return Array.from(this._hashData.values()).map(ContactMap.toDomainFromDTO);
  }

  async store(contact: Contact) {
    const contactToPersist = ContactMap.toDTO(contact);
    this._hashData.set(contactToPersist.id, contactToPersist);
    return ContactMap.toDomainFromDTO(contactToPersist);
  }

  async update(id: ContactId, updatedContact: Contact) {
    const documentToUpdate = this._hashData.get(id.id);

    if (!documentToUpdate) {
      throw new ContactNotFound();
    }

    const contact = ContactMap.toDTO(updatedContact);

    this._hashData.set(id.id, contact);

    return ContactMap.toDomainFromDTO(contact);
  }

  async remove(id: ContactId) {
    const removedContact = this._hashData.delete(id.id);

    if (!removedContact) {
      throw new ContactNotFound();
    }

    return;
  }
}

export class ContactNotFound extends Error {
  constructor() {
    super("Contact not found");
    Object.setPrototypeOf(this, ContactNotFound.prototype);
  }
}

import isEmail from "validator/lib/isEmail";
import { ContactValidationError } from "./ContactValidationError";

class Email {
  email: string;

  constructor(email: string) {
    if (!isEmail(email)) {
      throw new ContactValidationError("Invalid email");
    }

    this.email = email;
  }
}

export default Email;

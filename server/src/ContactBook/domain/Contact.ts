import ContactId from "./ContactId";
import Phone from "./Phone";
import Email from "./Email";

class Contact {
  constructor(
    readonly id: ContactId,
    readonly name: string,
    readonly lastName: string,
    readonly email: Email,
    readonly phone: Phone
  ) {}
}

export default Contact;

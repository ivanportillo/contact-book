import ContactId from "./ContactId";
import Contact from "./Contact";

export interface ContactRepository {
  byId(id: ContactId): Promise<Contact | null>;
  byEmail(email: string): Promise<Contact | null>;
  getAll(): Promise<Contact[]>;
  store(contact: Contact): Promise<Contact>;
  remove(id: ContactId): Promise<void>;
  update(id: ContactId, updatedContact: Contact): Promise<Contact>;
}

export class ContactValidationError extends Error {
  constructor(m: string) {
    super(m);
    Object.setPrototypeOf(this, ContactValidationError.prototype);
  }
}

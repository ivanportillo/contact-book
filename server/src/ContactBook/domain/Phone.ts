import isMobilePhone from "validator/lib/isMobilePhone";
import { ContactValidationError } from "./ContactValidationError";

class Phone {
  constructor(readonly prefix: string, readonly number: string) {
    if (!isMobilePhone(`${prefix}${number}`)) {
      throw new ContactValidationError("Invalid phone number");
    }
  }

  getFullPhone() {
    return `${this.prefix} ${this.number}`;
  }
}

export default Phone;

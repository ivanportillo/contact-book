class ContactId {
  constructor(readonly id: string) {
    this.id = id;
  }

  equals(contactId: ContactId) {
    return this.id === contactId.id;
  }
}

export default ContactId;

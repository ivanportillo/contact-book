import GetContact from "./GetContact";
import { ContactMemoryRepository } from "../infrastructure/ContactMemoryRepository";
import Contact from "../domain/Contact";
import ContactId from "../domain/ContactId";
import Email from "../domain/Email";
import Phone from "../domain/Phone";
import { ContactNotFound } from "../domain/ContactNotFound";

describe("GetContact application service", () => {
  const id = "1";
  const name = "Rick";
  const lastName = "Sanchez";
  const email = "wubbalubba@dubdub.com";
  const phonePrefix = "+34";
  const phoneNumber = "666777888";

  it("returns a contact given contact id", async () => {
    const contactRepository = new ContactMemoryRepository();
    const savedContact = new Contact(
      new ContactId(id),
      name,
      lastName,
      new Email(email),
      new Phone(phonePrefix, phoneNumber)
    );
    await contactRepository.store(savedContact);

    const interactor = new GetContact(contactRepository);

    const contact = await interactor.execute({ id: new ContactId(id) });

    expect(contact).toEqual(savedContact);
  });

  it("returns an error getting a contact given an invalid id", async () => {
    const contactRepository = new ContactMemoryRepository();
    const savedContact = new Contact(
      new ContactId(id),
      name,
      lastName,
      new Email(email),
      new Phone(phonePrefix, phoneNumber)
    );
    await contactRepository.store(savedContact);

    const interactor = new GetContact(contactRepository);

    await expect(
      interactor.execute({ id: new ContactId("InvalidId") })
    ).rejects.toThrow(ContactNotFound);
  });
});

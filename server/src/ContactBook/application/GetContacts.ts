import { ContactRepository } from "../domain/ContactRepository";

class GetContacts {
  _repository: ContactRepository;

  constructor(repository: ContactRepository) {
    this._repository = repository;
  }

  async execute() {
    return await this._repository.getAll();
  }
}

export default GetContacts;

import ContactId from "../domain/ContactId";
import { ContactRepository } from "../domain/ContactRepository";
import Contact from "../domain/Contact";
import Phone from "../domain/Phone";
import { EmailAlreadyUsedException } from "../domain/EmailAlreadyUsedException";
import Email from "../domain/Email";

class CreateContact {
  _repository: ContactRepository;

  constructor(repository: ContactRepository) {
    this._repository = repository;
  }

  async execute({
    id,
    name,
    lastName,
    email,
    phonePrefix,
    phoneNumber
  }: {
    id: string;
    name: string;
    lastName: string;
    email: string;
    phonePrefix: string;
    phoneNumber: string;
  }) {
    const existingContact = await this._repository.byEmail(email);

    if (existingContact) {
      throw new EmailAlreadyUsedException();
    }

    const contactToSave = new Contact(
      new ContactId(id),
      name,
      lastName,
      new Email(email),
      new Phone(phonePrefix, phoneNumber)
    );

    await this._repository.store(contactToSave);

    return contactToSave;
  }
}

export default CreateContact;

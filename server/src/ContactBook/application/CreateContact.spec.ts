import CreateContact from "./CreateContact";
import { ContactMemoryRepository } from "../infrastructure/ContactMemoryRepository";
import ContactMap from "../infrastructure/Mongo/ContactMap";
import Contact from "../domain/Contact";
import ContactId from "../domain/ContactId";
import Email from "../domain/Email";
import Phone from "../domain/Phone";
import { EmailAlreadyUsedException } from "../domain/EmailAlreadyUsedException";

describe("CreateContact application service", () => {
  const id = "1";
  const name = "Rick";
  const lastName = "Sanchez";
  const email = "wubbalubba@dubdub.com";
  const phonePrefix = "+34";
  const phoneNumber = "666777888";

  it("creates a contact given contact info", async () => {
    const contactRepository = new ContactMemoryRepository();

    const contactToCreate = {
      id,
      name,
      lastName,
      email,
      phonePrefix,
      phoneNumber
    };

    const interactor = new CreateContact(contactRepository);
    await interactor.execute(contactToCreate);

    const contacts = (await contactRepository.getAll()).map(ContactMap.toDTO);

    expect(contacts[0]).toEqual({
      id,
      name,
      lastName,
      email,
      phone: {
        prefix: phonePrefix,
        number: phoneNumber
      }
    });
  });

  it("returns an error creating a contact given an email already used", async () => {
    const contactRepository = new ContactMemoryRepository();

    const existingContact = new Contact(
      new ContactId(id),
      name,
      lastName,
      new Email(email),
      new Phone(phonePrefix, phoneNumber)
    );

    await contactRepository.store(existingContact);
    const interactor = new CreateContact(contactRepository);

    const contactToCreate = {
      id: "2",
      name: "Morty",
      lastName: "Smith",
      email,
      phonePrefix: "+34",
      phoneNumber: "666555444"
    };

    await expect(interactor.execute(contactToCreate)).rejects.toThrow(
      EmailAlreadyUsedException
    );
  });
});

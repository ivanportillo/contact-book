import GetContacts from "./GetContacts";
import { ContactMemoryRepository } from "../infrastructure/ContactMemoryRepository";
import Contact from "../domain/Contact";
import ContactId from "../domain/ContactId";
import Email from "../domain/Email";
import Phone from "../domain/Phone";

describe("GetContacts application service", () => {
  const id1 = "1";
  const id2 = "2";
  const name = "Rick";
  const lastName = "Sanchez";
  const email1 = "wubbalubba@dubdub.com";
  const email2 = "burp@dubdub.com";
  const phonePrefix = "+34";
  const phoneNumber1 = "666777888";
  const phoneNumber2 = "666555444";

  it("returns all contacts", async () => {
    const contactRepository = new ContactMemoryRepository();

    await contactRepository.store(
      new Contact(
        new ContactId(id1),
        name,
        lastName,
        new Email(email1),
        new Phone(phonePrefix, phoneNumber1)
      )
    );

    await contactRepository.store(
      new Contact(
        new ContactId(id2),
        name,
        lastName,
        new Email(email2),
        new Phone(phonePrefix, phoneNumber2)
      )
    );

    const savedContacts = await contactRepository.getAll();

    expect(savedContacts).toHaveLength(2);

    const interactor = new GetContacts(contactRepository);

    const contacts = await interactor.execute();

    expect(contacts).toHaveLength(2);
    expect(contacts).toEqual(savedContacts);
  });
});

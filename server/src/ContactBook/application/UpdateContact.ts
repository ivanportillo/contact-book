import { ContactRepository } from "../domain/ContactRepository";
import ContactId from "../domain/ContactId";
import { EmailAlreadyUsedException } from "../domain/EmailAlreadyUsedException";
import Contact from "../domain/Contact";
import Phone from "../domain/Phone";
import { ContactNotFound } from "../domain/ContactNotFound";
import Email from "../domain/Email";

class UpdateContact {
  _repository: ContactRepository;

  constructor(repository: ContactRepository) {
    this._repository = repository;
  }

  async execute(
    id: string,
    {
      name,
      lastName,
      email,
      phonePrefix,
      phoneNumber
    }: {
      name?: string;
      lastName?: string;
      email?: string;
      phonePrefix?: string;
      phoneNumber?: string;
    }
  ) {
    const contact = await this._repository.byId(new ContactId(id));

    if (!contact) {
      throw new ContactNotFound();
    }

    if (email) {
      const contactWithSameEmail = await this._repository.byEmail(email);

      if (contactWithSameEmail) {
        throw new EmailAlreadyUsedException();
      }
    }

    const updatedContact = new Contact(
      new ContactId(id),
      name ? name : contact.name,
      lastName ? lastName : contact.lastName,
      email ? new Email(email) : contact.email,
      phonePrefix && phoneNumber
        ? new Phone(phonePrefix, phoneNumber)
        : contact.phone
    );

    return await this._repository.update(new ContactId(id), updatedContact);
  }
}

export default UpdateContact;

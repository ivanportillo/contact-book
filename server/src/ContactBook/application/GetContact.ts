import ContactId from "../domain/ContactId";
import { ContactRepository } from "../domain/ContactRepository";
import { ContactNotFound } from "../domain/ContactNotFound";

class GetContact {
  _repository: ContactRepository;

  constructor(repository: ContactRepository) {
    this._repository = repository;
  }

  async execute({ id }: { id: ContactId }) {
    const contact = await this._repository.byId(id);
    if (!contact) {
      throw new ContactNotFound();
    }

    return contact;
  }
}

export default GetContact;

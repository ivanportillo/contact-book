import ContactId from "../domain/ContactId";
import { ContactRepository } from "../domain/ContactRepository";

class DeleteContact {
  _repository: ContactRepository;

  constructor(repository: ContactRepository) {
    this._repository = repository;
  }

  async execute({ id }: { id: ContactId }) {
    return await this._repository.remove(id);
  }
}

export default DeleteContact;

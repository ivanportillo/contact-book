import DeleteContact from "./DeleteContact";
import { ContactMemoryRepository } from "../infrastructure/ContactMemoryRepository";
import Contact from "../domain/Contact";
import ContactId from "../domain/ContactId";
import Email from "../domain/Email";
import Phone from "../domain/Phone";
import { ContactNotFound } from "../domain/ContactNotFound";

describe("DeleteContact application service", () => {
  const id = "1";
  const name = "Rick";
  const lastName = "Sanchez";
  const email = "wubbalubba@dubdub.com";
  const phonePrefix = "+34";
  const phoneNumber = "666777888";

  it("removes a contact given contact id", async () => {
    const contactRepository = new ContactMemoryRepository();

    await contactRepository.store(
      new Contact(
        new ContactId(id),
        name,
        lastName,
        new Email(email),
        new Phone(phonePrefix, phoneNumber)
      )
    );

    const interactor = new DeleteContact(contactRepository);

    expect(await contactRepository.getAll()).toHaveLength(1);

    await interactor.execute({ id: new ContactId(id) });

    expect(await contactRepository.getAll()).toHaveLength(0);
  });

  it("returns an error removing a contact given an invalid id", async () => {
    const contactRepository = new ContactMemoryRepository();

    await contactRepository.store(
      new Contact(
        new ContactId(id),
        name,
        lastName,
        new Email(email),
        new Phone(phonePrefix, phoneNumber)
      )
    );

    const interactor = new DeleteContact(contactRepository);

    expect(await contactRepository.getAll()).toHaveLength(1);

    await expect(
      interactor.execute({ id: new ContactId("InvalidId") })
    ).rejects.toThrow(ContactNotFound);
  });
});

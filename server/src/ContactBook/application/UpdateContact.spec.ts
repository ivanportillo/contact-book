import UpdateContact from "./UpdateContact";
import { ContactMemoryRepository } from "../infrastructure/ContactMemoryRepository";
import Contact from "../domain/Contact";
import Phone from "../domain/Phone";
import Email from "../domain/Email";
import ContactId from "../domain/ContactId";
import { ContactNotFound } from "../domain/ContactNotFound";
import { EmailAlreadyUsedException } from "../domain/EmailAlreadyUsedException";

describe("UpdateContact application service", () => {
  const id = "1";
  const name = "Rick";
  const lastName = "Sanchez";
  const email = "wubbalubba@dubdub.com";
  const phonePrefix = "+34";
  const phoneNumber = "666777888";

  it("updates a contact given new contact data", async () => {
    const contactRepository = new ContactMemoryRepository();

    await contactRepository.store(
      new Contact(
        new ContactId(id),
        name,
        lastName,
        new Email(email),
        new Phone(phonePrefix, phoneNumber)
      )
    );

    const interactor = new UpdateContact(contactRepository);

    const newLastName = "Smith";

    await interactor.execute(id, {
      lastName: newLastName
    });

    const updatedContact = await contactRepository.byId(new ContactId(id));
    expect(updatedContact?.lastName).toEqual(newLastName);
  });

  it("returns an error removing a contact given an invalid id", async () => {
    const contactRepository = new ContactMemoryRepository();

    await contactRepository.store(
      new Contact(
        new ContactId(id),
        name,
        lastName,
        new Email(email),
        new Phone(phonePrefix, phoneNumber)
      )
    );

    const interactor = new UpdateContact(contactRepository);

    const newLastName = "Smith";

    await expect(
      interactor.execute("Invalid ID", {
        lastName: newLastName
      })
    ).rejects.toThrow(ContactNotFound);
  });

  it("returns an error removing a contact given an existing email", async () => {
    const contactRepository = new ContactMemoryRepository();

    await contactRepository.store(
      new Contact(
        new ContactId(id),
        name,
        lastName,
        new Email(email),
        new Phone(phonePrefix, phoneNumber)
      )
    );

    const interactor = new UpdateContact(contactRepository);

    await expect(
      interactor.execute(id, {
        email
      })
    ).rejects.toThrow(EmailAlreadyUsedException);
  });
});

export class HttpErrorException extends Error {
  httpStatusCode;

  constructor(message: string, httpStatusCode: number) {
    super(message);
    this.httpStatusCode = httpStatusCode;
  }
}

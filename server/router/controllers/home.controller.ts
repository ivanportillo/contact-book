import { Request, Response } from "express";

export default {
  helloWorld: async (_req: Request, res: Response) => {
    return { hello: "world" };
  }
};

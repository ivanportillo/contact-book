import { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import CreateContact from "../../src/ContactBook/application/CreateContact";
import ContactMongoRepository from "../../src/ContactBook/infrastructure/ContactMongoRepository";
import ContactModelDocument from "../../src/ContactBook/infrastructure/Mongo/ContactModelDocument";
import ContactMap from "../../src/ContactBook/infrastructure/Mongo/ContactMap";
import GetContacts from "../../src/ContactBook/application/GetContacts";
import GetContact from "../../src/ContactBook/application/GetContact";
import ContactId from "../../src/ContactBook/domain/ContactId";
import DeleteContact from "../../src/ContactBook/application/DeleteContact";
import UpdateContact from "../../src/ContactBook/application/UpdateContact";
import { ContactNotFound } from "../../src/ContactBook/domain/ContactNotFound";
import { EmailAlreadyUsedException } from "../../src/ContactBook/domain/EmailAlreadyUsedException";
import { ContactValidationError } from "../../src/ContactBook/domain/ContactValidationError";
import { HttpErrorException } from "../../src/Core/domain/HttpErrorException";

export default {
  getContacts: async (_req: Request) => {
    const interactor = new GetContacts(
      new ContactMongoRepository(ContactModelDocument)
    );

    const contacts = await interactor.execute();

    return contacts.map(ContactMap.toDTO);
  },
  getContact: async (req: Request) => {
    try {
      const { id } = req.params;
      const interactor = new GetContact(
        new ContactMongoRepository(ContactModelDocument)
      );

      const contact = await interactor.execute({ id: new ContactId(id) });

      return contact ? ContactMap.toDTO(contact) : null;
    } catch (e) {
      if (e instanceof ContactNotFound) {
        throw new HttpErrorException(e.message, 404);
      }
    }
  },
  createContact: async (req: Request) => {
    try {
      const { name, lastName, email, phone } = req.body;

      const interactor = new CreateContact(
        new ContactMongoRepository(ContactModelDocument)
      );

      const id = uuidv4();

      const contact = await interactor.execute({
        id,
        name,
        lastName,
        email,
        phoneNumber: phone.number,
        phonePrefix: phone.prefix
      });

      return ContactMap.toDTO(contact);
    } catch (e) {
      if (
        e instanceof EmailAlreadyUsedException ||
        e instanceof ContactValidationError
      ) {
        throw new HttpErrorException(e.message, 400);
      }
    }
  },
  deleteContact: async (req: Request) => {
    try {
      const { id } = req.params;

      const interactor = new DeleteContact(
        new ContactMongoRepository(ContactModelDocument)
      );

      await interactor.execute({ id: new ContactId(id) });

      return null;
    } catch (e) {
      if (e instanceof ContactNotFound) {
        throw new HttpErrorException(e.message, 404);
      }
    }
  },
  updateContact: async (req: Request) => {
    try {
      const { id } = req.params;
      const { name, lastName, email, phonePrefix, phoneNumber } = req.body;

      const interactor = new UpdateContact(
        new ContactMongoRepository(ContactModelDocument)
      );

      const updatedContact = await interactor.execute(id, {
        name,
        lastName,
        email,
        phonePrefix,
        phoneNumber
      });

      return ContactMap.toDTO(updatedContact);
    } catch (e) {
      if (e instanceof ContactNotFound) {
        throw new HttpErrorException(e.message, 404);
      }

      if (
        e instanceof EmailAlreadyUsedException ||
        e instanceof ContactValidationError
      ) {
        throw new HttpErrorException(e.message, 400);
      }
    }
  }
};

import { Router } from "express";
import HomeController from "./controllers/home.controller";
import ContactsController from "./controllers/contacts.controller";
import responseBuilder from "../utils/responseBuilder";

export default (router: Router) => {
  const routes = [
    {
      path: "/api/",
      method: router.get,
      controller: HomeController.helloWorld
    },
    {
      path: "/api/contacts",
      method: router.get,
      controller: ContactsController.getContacts
    },
    {
      path: "/api/contacts",
      method: router.post,
      controller: ContactsController.createContact
    },
    {
      path: "/api/contacts/:id",
      method: router.get,
      controller: ContactsController.getContact
    },
    {
      path: "/api/contacts/:id",
      method: router.delete,
      controller: ContactsController.deleteContact
    },
    {
      path: "/api/contacts/:id",
      method: router.patch,
      controller: ContactsController.updateContact
    }
  ];

  routes.forEach(route => {
    const routeGenerator = route.method.bind(router);
    routeGenerator(route.path, responseBuilder(route.controller));
  });

  return router;
};

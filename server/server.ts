import express, { Application } from "express";
import helmet from "helmet";
import path from "path";
import router from "./router";
import expressPromiseRouter from "express-promise-router";

const server = express();

server.use(express.static(path.join(__dirname, "../../public")));
server.use(helmet());

server.use(express.json({ limit: "50mb" }));
server.use(express.urlencoded({ limit: "50mb", extended: true }));

export default (): Application => {
  server.use(router(expressPromiseRouter()));

  //@ts-ignore
  server.use((error, _req, res, _next) => {
    console.error(error);

    return res
      .status(error?.response?.status || 500)
      .send({ error: error?.response?.message });
  });

  return server;
};

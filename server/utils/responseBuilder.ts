import { Request, Response } from "express";
import { HttpErrorException } from "../src/Core/domain/HttpErrorException";

export default (controllerToHandle: Function) => async (
  req: Request,
  res: Response
) => {
  try {
    const response = await controllerToHandle(req, res);

    return res.status(200).json({
      success: true,
      data: response
    });
  } catch (e) {
    const errorResponse = {
      success: false,
      error: e.message
    };

    if (e instanceof HttpErrorException) {
      return res.status(e.httpStatusCode).json(errorResponse);
    }

    return res.status(500).json(errorResponse);
  }
};

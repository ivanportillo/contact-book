describe("API Functional tests", () => {
  before(() => {
    cy.request("GET", "http://localhost:3000/api/contacts").then(response => {
      const contacts = response.body.data;
      contacts.forEach(contact => {
        cy.request(
          "DELETE",
          `http://localhost:3000/api/contacts/${contact.id}`
        );
      });
    });
  });

  it("Creates a contact", () => {
    cy.request("POST", "http://localhost:3000/api/contacts", {
      name: "Rick",
      lastName: "Sanchez",
      email: "wubbalubba@dubdub.com",
      phone: {
        prefix: "+34",
        number: "666888999"
      }
    }).then(response => {
      expect(response.body).to.have.property("success", true);
      expect(response.body.data).to.have.property("name", "Rick");
    });
  });

  it("Get contacts", () => {
    cy.request("GET", "http://localhost:3000/api/contacts").then(response => {
      expect(response.body).to.have.property("success", true);
      expect(response.body.data).to.have.lengthOf(1);

      const contacts = response.body.data;

      cy.request(
        "GET",
        `http://localhost:3000/api/contacts/${contacts[0].id}`
      ).then(response => {
        expect(response.body).to.have.property("success", true);
      });
    });
  });

  it("Get all contacts", () => {
    cy.request("GET", "http://localhost:3000/api/contacts").then(response => {
      expect(response.body).to.have.property("success", true);
      expect(response.body.data).to.have.lengthOf(1);
    });
  });

  it("Updates a contact", () => {
    cy.request("GET", "http://localhost:3000/api/contacts").then(response => {
      expect(response.body).to.have.property("success", true);
      expect(response.body.data).to.have.lengthOf(1);

      const contacts = response.body.data;

      cy.request(
        "PATCH",
        `http://localhost:3000/api/contacts/${contacts[0].id}`,
        {
          lastName: "Smith"
        }
      ).then(response => {
        expect(response.body).to.have.property("success", true);
        expect(response.body.data).to.have.property("lastName", "Smith");
      });
    });
  });

  it("Removes a contact", () => {
    cy.request("GET", "http://localhost:3000/api/contacts").then(response => {
      expect(response.body).to.have.property("success", true);
      expect(response.body.data).to.have.lengthOf(1);

      const contacts = response.body.data;

      cy.request(
        "DELETE",
        `http://localhost:3000/api/contacts/${contacts[0].id}`
      ).then(response => {
        expect(response.body).to.have.property("success", true);
      });
    });
  });
});

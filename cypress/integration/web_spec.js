describe("Functional tests", function() {
  before(function() {
    cy.request("GET", "http://localhost:3000/api/contacts").then(response => {
      const contacts = response.body.data;
      contacts.forEach(contact => {
        cy.request(
          "DELETE",
          `http://localhost:3000/api/contacts/${contact.id}`
        );
      });
    });
  });

  it("Creates a contact", function() {
    cy.visit("http://localhost:3000/");

    cy.get("[data-functional-id=add-contact-button]").click();
    cy.wait(100);

    cy.get("[data-functional-id=name-input]").type("Rick");

    cy.get("[data-functional-id=last-name-input]").type("Sanchez");

    cy.get("[data-functional-id=email-input]").type("rick@sanchez.com");

    cy.get("[data-functional-id=prefix-input]").type("+34");

    cy.get("[data-functional-id=number-input]").type("666777888");

    cy.get("[data-functional-id=form-submit]").click();
    cy.wait(100);

    cy.url().should("include", "/contact/");
  });

  it("Updates a contact", () => {
    cy.visit("http://localhost:3000/");

    cy.get("[data-functional-id=contact-0]").click();

    cy.wait(100);

    cy.get("[data-functional-id=contact-edit]").click();
    cy.wait(100);

    cy.get("[data-functional-id=last-name-input]").clear();
    cy.get("[data-functional-id=last-name-input]").type("Smith");

    cy.get("[data-functional-id=form-submit]").click();
    cy.wait(100);

    cy.url().should("include", "/contact/");

    cy.get("[data-functional-id=contact-full-name]").should($b => {
      expect($b).to.contain("Rick Smith");
    });
  });

  it("Removes a contact", () => {
    cy.visit("http://localhost:3000/");

    cy.get("[data-functional-id=contact-0]").click();

    cy.wait(100);

    cy.get("[data-functional-id=remove-button]").click();
    cy.get("[data-functional-id=remove-button]").click();

    cy.wait(100);

    cy.url().should("include", "/");
  });
});

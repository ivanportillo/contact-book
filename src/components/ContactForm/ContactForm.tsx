import styled from "styled-components";
import { Formik } from "formik";
import Axios from "axios";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import Header from "../Header/Header";
import Input from "../Input/Input";
import { IContact } from "../../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";

const Form = styled.form`
  padding: 40px;
  display: flex;
  flex-direction: column;
`;

const PhoneForm = styled.div`
  margin: 10px 0 10px 0;
  display: flex;
  flex-direction: column;
`;

const SubmitButton = styled.input`
  padding: 10px;
  font-size: 15px;
`;

const FormInput = styled(Input)`
  margin-bottom: 5px;
`;

interface ContactFormProps {
  contact?: IContact;
}

const createNewContact = async (
  name: string,
  lastName: string,
  email: string,
  phonePrefix: string,
  phoneNumber: string
) => {
  return Axios.post(
    "/api/contacts",
    {
      name,
      lastName,
      email,
      phone: {
        prefix: phonePrefix,
        number: phoneNumber
      }
    },
    {
      baseURL: "http://localhost:3000"
    }
  );
};

const updateContact = async (
  id: string,
  name: string,
  lastName: string,
  phonePrefix: string,
  phoneNumber: string,
  email?: string
) => {
  return Axios.patch(
    `/api/contacts/${id}`,
    {
      name,
      lastName,
      email,
      phone: {
        prefix: phonePrefix,
        number: phoneNumber
      }
    },
    {
      baseURL: "http://localhost:3000"
    }
  );
};

const ContactForm: React.FunctionComponent<ContactFormProps> = ({
  contact
}) => {
  const router = useRouter();
  return (
    <>
      <Header title={contact ? "Edit contact" : "Add a new contact"} />
      <Formik
        initialValues={{
          name: contact ? contact.name : "",
          lastName: contact ? contact.lastName : "",
          email: contact ? contact.email : "",
          phonePrefix: contact ? contact.phone.prefix : "",
          phoneNumber: contact ? contact.phone.number : ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          const { name, lastName, email, phonePrefix, phoneNumber } = values;
          if (!contact) {
            createNewContact(name, lastName, email, phonePrefix, phoneNumber)
              .then(result => {
                if (result.data.success) {
                  const { id } = result.data.data;
                  router.push(`/contact/${id}`);
                  toast.success("Contact added successfully");
                }
              })
              .catch(result => {
                toast.error(result.response.data.error);
                setSubmitting(false);
              });
          } else {
            const newEmail = contact.email === email ? undefined : email;
            updateContact(
              contact.id,
              name,
              lastName,
              phonePrefix,
              phoneNumber,
              newEmail
            )
              .then(result => {
                if (result.data.success) {
                  const { id } = result.data.data;
                  router.push(`/contact/${id}`);
                  toast.success("Contact updated successfully");
                }
              })
              .catch(result => {
                toast.error(result.response.data.error);
                setSubmitting(false);
              });
          }
        }}
      >
        {({
          values,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
        }) => (
          <Form
            onSubmit={e => {
              e.preventDefault();
              handleSubmit(e);
            }}
          >
            <FormInput
              placeholder="Name"
              name="name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.name}
              error={errors.name}
              functionalId="name-input"
            />
            <FormInput
              placeholder="Last name"
              name="lastName"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.lastName}
              error={errors.lastName}
              functionalId="last-name-input"
            />
            <FormInput
              placeholder="Email"
              type="email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              error={errors.email}
              functionalId="email-input"
            />
            <PhoneForm>
              <FormInput
                placeholder="Phone prefix"
                name="phonePrefix"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phonePrefix}
                error={errors.phonePrefix}
                functionalId="prefix-input"
              />
              <FormInput
                placeholder="Phone number"
                name="phoneNumber"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phoneNumber}
                error={errors.phoneNumber}
                functionalId="number-input"
              />
            </PhoneForm>
            <SubmitButton
              type="submit"
              data-functional-id="form-submit"
              value="Add"
              disabled={isSubmitting}
            />
          </Form>
        )}
      </Formik>
    </>
  );
};

export default ContactForm;

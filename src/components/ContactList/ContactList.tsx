import { FunctionComponent, useState } from "react";
import styled from "styled-components";

import { IContact } from "../../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";
import ContactItem from "./ContactItem";

interface ContactListProps {
  contacts: IContact[];
}

const SearchInput = styled.input`
  padding: 14px 8px;
  border-bottom: 1px solid #eaeaea;
  font-size: 14px;
`;

const Container = styled.p`
  padding: 15px;
  font-size: 20px;
`;

const ContactList: FunctionComponent<ContactListProps> = ({ contacts }) => {
  const [filter, setFilter] = useState("");

  const filteredContacts = contacts.filter(contact =>
    contact.name.toLowerCase().includes(filter.toLowerCase())
  );

  return contacts.length ? (
    <>
      <SearchInput
        placeholder="Search a contact..."
        onChange={e => setFilter(e.target.value)}
      />
      <ul>
        {filteredContacts.map((contact, index) => (
          <li>
            <ContactItem
              functionalId={`contact-${index}`}
              key={contact.name}
              contact={contact}
            />
          </li>
        ))}
      </ul>
    </>
  ) : (
    <Container>No contacts</Container>
  );
};

export default ContactList;

import { FunctionComponent } from "react";
import Link from "next/link";
import { IContact } from "../../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";
import styled from "styled-components";

const Container = styled.div`
  height: 100%;
  padding: 14px 8px;
  border-bottom: 1px solid #eaeaea;
  font-size: 14px;
  cursor: pointer;
`;

interface ContactItemProps {
  contact: IContact;
  functionalId?: string;
}

const ContactItem: FunctionComponent<ContactItemProps> = ({
  contact,
  functionalId
}) => (
  <Link href="/contact/[id]/" as={`/contact/${contact.id}`}>
    <Container data-functional-id={functionalId}>{contact.name}</Container>
  </Link>
);

export default ContactItem;

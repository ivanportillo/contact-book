import { FunctionComponent } from "react";
import styled from "styled-components";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

import { IContact } from "../../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";
import Header from "../Header/Header";
import RemoveButton from "../RemoveButton/RemoveButton";
import Axios from "axios";
import Link from "next/link";

interface ContactDetailProps {
  contact: IContact;
}

const Content = styled.div`
  display: flex;
  justify-content: center;
`;

const Detail = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 24px;
  padding: 40px;
`;

const Title = styled.span`
  font-size: 34px;
`;

const removeContact = async (id: string) => {
  return Axios.delete(`/api/contacts/${id}`, {
    baseURL: "http://localhost:3000"
  });
};

const ContactDetail: FunctionComponent<ContactDetailProps> = ({ contact }) => {
  const router = useRouter();
  return (
    <>
      <Header
        rightElement={
          <Link
            href={`/contact/${contact.id}/edit`}
            as={`/contact/${contact.id}/edit`}
          >
            <a data-functional-id="contact-edit">Edit</a>
          </Link>
        }
      />
      <Content>
        <Detail>
          <Title>
            <b data-functional-id="contact-full-name">{`${contact.name} ${contact.lastName}`}</b>
          </Title>
          <span>✉️ {contact.email}</span>
          <span>📞 {`${contact.phone.prefix} ${contact.phone.number}`}</span>
          <RemoveButton
            functionalId="remove-button"
            onClick={() => {
              removeContact(contact.id)
                .then(result => {
                  if (result.data.success) {
                    toast.success("Contact removed!");
                    router.push("/");
                  }
                })
                .catch(result => {
                  toast.error(result.response.data.error);
                });
            }}
          />
        </Detail>
      </Content>
    </>
  );
};

export default ContactDetail;

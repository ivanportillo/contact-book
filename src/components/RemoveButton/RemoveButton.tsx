import { useState, ButtonHTMLAttributes } from "react";
import styled from "styled-components";

const Button = styled.button`
  padding: 5px;
  margin-top: 10px;
  ${(props: { isClicked: boolean }) =>
    props.isClicked && `background-color: red; color: white;`}
`;

interface CustomButtonProps {
  functionalId?: string;
}

const RemoveButton: React.FunctionComponent<ButtonHTMLAttributes<
  HTMLButtonElement
> &
  CustomButtonProps> = ({ onClick, functionalId }) => {
  const [firstClick, clickFirstTime] = useState(false);

  return (
    <Button
      isClicked={firstClick}
      data-functional-id={functionalId}
      onClick={e => {
        if (!firstClick) {
          clickFirstTime(true);
        } else {
          if (onClick) onClick(e);
        }
      }}
    >
      {firstClick ? "Are you sure?" : "Remove"}
    </Button>
  );
};

export default RemoveButton;

import styled from "styled-components";
import { NextPage } from "next";
import Link from "next/link";
import ContactList from "../ContactList/ContactList";
import { IContact } from "../../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";

const Menu = styled.ul`
  display: flex;
  flex-direction: column;
  flex: 0.3;
  border-right: 1px solid #eaeaea;
`;

const Content = styled.div`
  display: flex;
  max-width: 1000px;
  width: 100%;
  margin: 0 auto;
  height: 100%;
  border-right: 1px solid #eaeaea;
  border-left: 1px solid #eaeaea;
  background-color: white;
`;

const Header = styled.header`
  display: flex;
  background-color: #fafafa;
  justify-content: space-between;
  align-items: center;
  height: 50px;
  border-bottom: 1px solid #eaeaea;
  font-weight: 800;
  font-size: 16px;
  padding: 20px;
`;

const RightElement = styled.div`
  display: flex;
  flex: 0.7;
  flex-direction: column;
`;

const EmptySpace = styled.div`
  width: 0.1px;
`;

const AddressBook: NextPage<{
  contacts?: IContact[];
}> = ({ contacts = [], children }) => (
  <Content>
    <Menu>
      <Header>
        <EmptySpace />
        <>📙 CONTACTS</>
        <Link href="/new-contact" as="/new-contact">
          <a data-functional-id="add-contact-button">+</a>
        </Link>
      </Header>
      <ContactList contacts={contacts} />
    </Menu>
    <RightElement>{children}</RightElement>
  </Content>
);

export default AddressBook;

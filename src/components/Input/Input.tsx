import styled from "styled-components";
import { InputHTMLAttributes } from "react";

interface CustomInputProps {
  error?: string;
  functionalId?: string;
}

const FormInput = styled.input`
  border: 1px solid grey;
  padding: 10px;
  font-size: 15px;
`;

const Error = styled.div``;

const Input: React.FunctionComponent<CustomInputProps &
  InputHTMLAttributes<HTMLInputElement>> = ({
  error,
  functionalId,
  ...rest
}) => (
  <>
    <FormInput
      data-functional-id={functionalId}
      placeholder="Name"
      name="name"
      {...rest}
    />
    {error && <Error>{error}</Error>}
  </>
);

export default Input;

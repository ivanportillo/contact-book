import styled from "styled-components";
import Link from "next/link";

interface HeaderProps {
  title?: string;
  rightElement?: JSX.Element;
}

const StyledHeader = styled.header`
  height: 50px;
  display: flex;
  padding: 10px;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #eaeaea;
  font-weight: 800;
  font-size: 24px;
  color: #666;
`;

const EmptySpace = styled.div`
  width: 0.1px;
`;

const Header: React.FunctionComponent<HeaderProps> = ({
  title,
  rightElement
}) => (
  <StyledHeader>
    {rightElement ? (
      <>
        <Link href="/" as="/">
          <a>X</a>
        </Link>
        <EmptySpace />
        {rightElement}
      </>
    ) : (
      <>
        <Link href="/" as="/">
          <a>X</a>
        </Link>
        {title && <div>{title}</div>}
        {title && <EmptySpace />}
      </>
    )}
  </StyledHeader>
);

export default Header;

import Head from "next/head";
import styled from "styled-components";
import { NextPage } from "next";
import { IContact } from "../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";
import AddressBook from "../components/AddressBook/AddressBook";
import Axios from "axios";

const Container = styled.div`
  display: flex;
  height: 100%;
  background-color: #eaeaea;
`;

const Welcome = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  height: 100%;
`;

const Home: NextPage<{ contacts: IContact[] }> = ({ contacts }) => (
  <>
    <Head>
      <title>ContactBook</title>
      <link rel="icon" href="/favicon.ico" />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"
      />
    </Head>

    <Container>
      <AddressBook contacts={contacts}>
        <Welcome>Welcome to the contact book</Welcome>
      </AddressBook>
    </Container>
  </>
);

Home.getInitialProps = async () => {
  const {
    data: { data }
  } = await Axios.get("/api/contacts", {
    baseURL: "http://localhost:3000"
  });

  return {
    contacts: data
  };
};

export default Home;

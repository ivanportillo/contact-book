import Head from "next/head";
import styled from "styled-components";
import { NextPage } from "next";
import Axios from "axios";
import AddressBook from "../../../../components/AddressBook/AddressBook";
import { IContact } from "../../../../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";
import { useRouter } from "next/router";
import ContactDetail from "../../../../components/ContactDetail/ContactDetail";
import ContactForm from "../../../../components/ContactForm/ContactForm";

const Container = styled.div`
  display: flex;
  height: 100%;
  background-color: #eaeaea;
`;

const EditContact: NextPage<{ contacts: IContact[] }> = ({ contacts }) => {
  const { query } = useRouter();
  const { id } = query;

  const selectedContact = contacts.find(contact => contact.id === id);

  if (!selectedContact) {
    throw new Error("Contact not found");
  }

  return (
    <>
      <Head>
        <title>ContactBook</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"
        />
      </Head>

      <Container>
        <AddressBook contacts={contacts}>
          <ContactForm contact={selectedContact} />
        </AddressBook>
      </Container>
    </>
  );
};

EditContact.getInitialProps = async ({ query }) => {
  const {
    data: { data }
  } = await Axios.get("/api/contacts", {
    baseURL: "http://localhost:3000"
  });

  return {
    contacts: data
  };
};

export default EditContact;

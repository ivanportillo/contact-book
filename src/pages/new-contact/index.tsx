import Head from "next/head";
import styled from "styled-components";
import { NextPage } from "next";
import Axios from "axios";
import { IContact } from "../../../server/src/ContactBook/infrastructure/Mongo/ContactModelDocument";
import AddressBook from "../../components/AddressBook/AddressBook";
import ContactForm from "../../components/ContactForm/ContactForm";

const Container = styled.div`
  display: flex;
  height: 100%;
  background-color: #eaeaea;
`;

const NewContact: NextPage<{ contacts: IContact[] }> = ({ contacts }) => {
  return (
    <>
      <Head>
        <title>ContactBook</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"
        />
      </Head>

      <Container>
        <AddressBook contacts={contacts}>
          <ContactForm />
        </AddressBook>
      </Container>
    </>
  );
};

NewContact.getInitialProps = async () => {
  const {
    data: { data }
  } = await Axios.get("/api/contacts", {
    baseURL: "http://localhost:3000"
  });

  return {
    contacts: data
  };
};

export default NewContact;

import { AppProps } from "next/app";
import { createGlobalStyle } from "styled-components";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const StyledReset = createGlobalStyle`
html,
body {
  padding: 0;
  margin: 0;
  height: 100%;
  position: relative;
  width: 100%;
  font-feature-settings: 'kern';
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
}

*,
*::before,
*::after {
  box-sizing: border-box;
}

a,
a:active,
a:hover,
a:link,
a:visited {
  color: inherit;
  text-decoration: none;
}


button,
input,
textarea {
  font-family: inherit;
  font-size: inherit;
  font-weight: inherit;
  margin: 0;
  padding: 0;
  border: 0;
}

input:focus,
textarea:focus,
select:focus {
  outline: none;
}

figure {
  margin: 0;
  padding: 0;
}

html {
  font-size: 10px;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  margin: 0;
}

ol,
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}

p {
  margin: 0;
}

#__next { height: 100%; }
`;

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <StyledReset />
      <Component {...pageProps} />
      <ToastContainer />
    </>
  );
}

export default MyApp;

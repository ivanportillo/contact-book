# ContactBook :orange_book:

### Prerequisites

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Getting Started

Run the project for development :

```
docker-compose up
```

## Running the tests

Launch commands inside or outside the container

```
yarn run test
```

## Functional tests

```
yarn run cypress:open
```
